@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Create a new Thread</h3>
                        <hr>
                    </div>

                    <div class="panel-body">
                        <form method="POST" action="/threads">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="channel">Choose a Channel</label>
                                <select class="form-control" name="channel_id">
                                    <option value="">Select a channel...</option>
                                    @foreach ($channels as $channel)
                                        <option value="{{ $channel->id }}" 
                                            {{ old('channel_id') == $channel->id ? 'selected' : '' }}>
                                                {{ $channel->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="title">Title: </label>
                                <input type="text" name="title" class="form-control" value="{{ old('title') }}">
                            </div>

                            <div class="form-group">
                                <label for="body">Body: </label>
                                <textarea name="body" id="body" class="form-control" rows="8">{{ old('body') }}</textarea>
                            </div>

                            <button class="btn btn-primary" type="submit">Publish</button>
                        </form>
                        
                        @if($errors->any())
                            <br>
                            <div class="alert alert-danger" role="alert">
                                    
                              <ul>
                                @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                                @endforeach
                              </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
