@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <a href="#">{{ $thread->creator->name }}</a> Posted : 
                  {{ $thread->title }}</div>

                <div class="card-body">
                   {{ $thread->body }}
                </div>
            </div>

        <br> <br>

            @foreach ($replies as $reply)
              @include('threads.reply')
            @endforeach
            {{ $replies->links() }}
            @if ( auth()->check() )
                    <form method="POST" action="{{ $thread->path() . '/replies' }}" class="form">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <textarea class="form-control" name="body" rows="5" placeholder="What are your thoughts?"></textarea>
                        </div>
                        <button class="btn btn-primary">Post</button> 
                    </form>
                    @if($errors->any())
                        <br>
                        <div class="alert alert-danger" role="alert">
                                
                          <ul>
                            @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                            @endforeach
                          </ul>
                        </div>
                    @endif
            @else 
                <p class="text-center">Please <a href="{{ route('login') }}">Login here</a> to post your thoughts...</p>
            @endif
        </div> <!-- col-md-8 -->
        
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <p>
                    This thread was published {{ $thread->created_at->diffForHumans() }}
                    by <a href="#">{{ $thread->creator->name }}</a>, <br> 
                    and currently has {{ $thread->replies_count }} 
                    {{ str_plural('comment', $thread->replies_count) }}.
                    </p>
                </div>
            </div>
        </div>

    </div>

    

</div>
@endsection
