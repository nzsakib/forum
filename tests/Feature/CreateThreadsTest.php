<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Thread;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CreateThreadsTest extends TestCase
{
    use DatabaseMigrations;
    
    /** @test */
    function guests_may_not_create_threads()
    {
        // $this->expectException('Illuminate\Auth\AuthenticationException');
        $this->get('threads/create')
            ->assertRedirect('/login');
        $this->post('/threads', [])
            ->assertRedirect('/login');

    }

    /** @test */
    function an_authenticated_user_can_create_forum_threads()
    {
        // Given a signedin user 
        $this->signIn();
        // when we hit endpoint to create a new thread 
        $thread = make('App\Thread');

        $response = $this->post('/threads', $thread->toArray());
        
        // When we visit thread page, see the new thread
        $this->get($response->headers->get('Location'))
            ->assertSee($thread->title)
            ->assertSee($thread->body);
    }

    /** @test */
    function a_thread_requires_a_title()
    {
        $this->publishThread(['title' => null])
            ->assertSessionHasErrors('title');
    }

    /** @test */
    function a_thread_requires_a_body()
    {
        $this->publishThread(['body' =>null])
            ->assertSessionHasErrors('body');
    }

    function publishThread($overrides = [])
    {
        $this->signIn();

        $thread = make(Thread::class, $overrides);

        return $this->post('/threads', $thread->toArray());
    }

    /** @test */
    function a_thread_requires_a_valid_channel()
    {
        factory('App\Channel', 2)->create();

        $this->publishThread(['channel_id' => null])
            ->assertSessionHasErrors('channel_id');

        $this->publishThread(['channel_id' => 9999])
            ->assertSessionHasErrors('channel_id');
    }
}
